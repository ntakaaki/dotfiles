set tabstop=4
set is
set nu
set shiftwidth=4
syntax on
set wildmode=list:longest
set nocompatible               " be iMproved
filetype off                   " required!
filetype plugin indent off     " required!

highlight LineNr ctermfg=darkyellow    " 行番号
highlight NonText ctermfg=darkgrey
highlight Folded ctermfg=blue
highlight SpecialKey cterm=underline ctermfg=darkgrey

highlight ZenkakuSpace cterm=underline ctermfg=lightblue guibg=white
match ZenkakuSpace /　/

" 日本語を含まない場合は fileencoding に encoding を使うようにする
if has('autocmd')
  function! AU_ReCheck_FENC()
    if &fileencoding =~# 'iso-2022-jp' && search("[^\x01-\x7e]", 'n') == 0
      let &fileencoding=&encoding
    endif
  endfunction
  autocmd BufReadPost * call AU_ReCheck_FENC()
endif

" 改行コードの自動認識
set fileformats=unix,dos,mac

" □とか○の文字があってもカーソル位置がずれないようにする
if exists('&ambiwidth')
  set ambiwidth=double
endif

" NeoBundle Setting

if has('vim_starting')
  set runtimepath+=~/.vim/neobundle.vim/
  call neobundle#rc(expand('~/.vim/'))
endif
" let NeoBundle manage NeoBundle
" required! 
NeoBundle 'Shougo/neobundle.vim'
let g:neobundle#log_filename=expand('~/.vim/plugin_install.log')

" recommended to install
NeoBundle 'Shougo/vimproc'
" after install, turn shell ~/.vim/bundle/vimproc, (n,g)make -f your_machines_makefile
NeoBundle 'Shougo/vimshell'
NeoBundle 'Shougo/unite.vim'
NeoBundle 'git://github.com/Shougo/neocomplcache.git', {'directory': 'neocomplcache'}
NeoBundle 'git://github.com/kana/vim-smartchr.git', {'directory': 'smartchr'}
NeoBundle 'git://github.com/scrooloose/nerdtree.git',  {'directory': 'nerdtree'}
NeoBundle 'git://github.com/tpope/vim-surround.git',  {'directory': 'surround'}
NeoBundle 'git://github.com/vim-scripts/sudo.vim.git',  {'directory': 'sudo'}
NeoBundle 'pangloss/vim-javascript'

" My Bundles here:
"
" original repos on github
NeoBundle "mattn/emmet-vim"
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'Lokaltog/vim-easymotion'
NeoBundle 'rstacruz/sparkup', {'rtp': 'vim/'}
" vim-scripts repos
NeoBundle 'L9'
NeoBundle 'FuzzyFinder'

" non github repos
"NeoBundle 'git://git.wincent.com/command-t.git'
" non git repos
NeoBundle 'http://svn.macports.org/repository/macports/contrib/mpvim/'
NeoBundle 'https://bitbucket.org/ns9tks/vim-fuzzyfinder'

NeoBundle 'JavaScript-syntax'
NeoBundle 'pangloss/vim-javascript'
NeoBundle 'git://github.com/kchmck/vim-coffee-script.git'
NeoBundle 'thinca/vim-quickrun'
NeoBundle 'ruby-matchit'
NeoBundle 'Shougo/neosnippet'
NeoBundle 'scrooloose/syntastic.git'
" NeoBundle 'taichouchou2/vim-rsense'

" コメント
NeoBundle 'tomtom/tcomment_vim'
"
" " railsサポート
NeoBundle 'taichouchou2/vim-rails'
NeoBundle 'romanvbabenko/rails.vim'
NeoBundle 'ujihisa/unite-rake'
NeoBundle 'basyura/unite-rails'
"
" " reference環境
NeoBundle 'thinca/vim-ref'
NeoBundle 'taichouchou2/vim-ref-ri'
NeoBundle 'taq/vim-rspec'

" git support
NeoBundle 'motemen/git-vim'

" Redmine Setting
NeoBundle 'mattn/webapi-vim'
NeoBundle 'kana/vim-metarw'
NeoBundle 'mattn/vim-metarw-redmine'

let g:metarw_redmine_server = 'https://hq.newsfiltr.net'
let g:metarw_redmine_apikey = '5537f7770b78889365286ad2cdb15f246cff2962'

" SCSS Support
NeoBundle 'cakebaker/scss-syntax.vim'

" color cheme
NeoBundle 'nanotech/jellybeans.vim'
NeoBundle 'w0ng/vim-hybrid'
NeoBundle 'vim-scripts/twilight'
NeoBundle 'jonathanfilip/vim-lucius'
NeoBundle 'jpo/vim-railscasts-theme'
NeoBundle 'altercation/vim-colors-solarized'
NeoBundle 'vim-scripts/Wombat'
NeoBundle 'tomasr/molokai'
NeoBundle 'vim-scripts/rdark'

" ...

" vimshell setting
let g:vimshell_interactive_update_time = 10
nnoremap <silent> vsl :VimShell<CR>
nnoremap <silent> vsc :VimShellCreate<CR>
nnoremap <silent> vsp :VimShellPop<CR>

" smartchr setting
autocmd FileType php inoremap <expr> = smartchr#loop(' = ', ' == ', ' === ', ' => ', '=')
autocmd FileType php inoremap <expr> , smartchr#one_of(', ', ',')
autocmd FileType php inoremap <expr> . smartchr#one_of('.', ' array')

" nerd tree setting
map <leader>d :execute 'NERDTreeToggle ' . getcwd()<CR>

let g:ref_phpmanual_path = '/home/noguchi/docs/php-chunked-xhtml/'
let g:ref_phpmanual_cmd  = 'w3m -dump %s'
nmap ,ra :<C-u>Ref alc<Space>
let g:ref_alc_start_linenumber = 39

" neocomplcache setting
let g:neocomplcache_enable_at_startup = 1
let g:neocomplcache_enable_smart_case = 1
let g:neocomplcache_enable_camel_case_completion = 1
let g:neocomplcache_enable_underbar_completion = 1
let g:neocomplcache_min_syntax_length = 3
let g:neocomplcache_lock_buffer_name_pettern = '*ku*'
"let g:neocomplcache_dictionary_filetype_lists = {
"let g:neocomplcache_keyword_patterns['default'] = '\h\w*'

"neonipeet
imap <C-k> <Plug>(neosnippet_expand_or_jump)
smap <C-k> <Plug>(neosnippet_expand_or_jump)

filetype indent on
filetype plugin indent on     " required!
filetype plugin on
"
" Brief help
" :NeoBundleList          - list configured bundles
" :NeoBundleInstall(!)    - install(update) bundles
" :NeoBundleClean(!)      - confirm(or auto-approve) removal of unused bundles

"quickrun
let g:quickrun_config = {}
let g:quickrun_config._ = {'runner' : 'vimproc'}
"let g:quickrun_config['ruby.rspec'] = { 'command': 'rspec', 'exec': '%o %c %s' }
" let g:quickrun_config['ruby.rspec'] = {'command': 'rspec', 'cmdopt': '--format progress -I .', 'exec': ['bundle exec %c %o %s %a'], 'filetype': 'rspec-result'}

" bundleを利用する設定。
let g:quickrun_config['rspec/bundle'] = {
			\ 'type': 'rspec/bundle',
			\ 'command': 'rspec',
			\ 'exec': 'bundle exec %c %s',
			\ 'outputter/buffer/filetype': 'rspec-result',
			\ 'split': ''
			\}

" 通常で利用する設定。
let g:quickrun_config['rspec/normal'] = {
			\ 'type': 'rspec/normal',
			\ 'command': 'rspec',
			\ 'exec': '%c %s',
			\ 'outputter/buffer/filetype': 'rspec-result',
			\ 'vsplit': ''
			\}

"※ここで動作を変更する
"どの場面で使うか（rubyかrailsか）によってコマンドが変わるので
"手動でコメントアウトをつけたり外したりして利用している。
function! RSpecQuickrun()
	" 通常の場合。（rubyなど）
	let b:quickrun_config = {'type': 'rspec/normal'}

	" bundle、Gemfileを利用している場合。（railsなど）
	" let b:quickrun_config = {'type': 'rspec/bundle'}
endfunction

autocmd BufReadPost *_spec.rb call RSpecQuickrun()

augroup RSpec
	autocmd!
	autocmd BufWinEnter,BufNewFile *_spec.rb set filetype=ruby.rspec
augroup END

" syntastic
let g:syntastic_mode_map = { 'mode': 'active',
  \ 'active_filetypes': [], 
  \ 'passive_filetypes': ['html', 'javascript'] }
let g:syntastic_auto_loc_list = 1 
let g:syntastic_javascript_checker = 'gjslint'

let php_sql_query=1
let php_htmlInStrings=1
let php_folding=1
set makeprg=php\ -l\ %
set errorformat=%m\ in\ %f\ on\ line\ %l
function PHPLint()
        let result = system( &ft . ' -l ' . bufname(""))
        echo result
endfunction
"Escの2回押しでハイライト消去
nmap <ESC><ESC> ;nohlsearch<CR><ESC>

autocmd FileType php set tabstop=2 tw=0 sw=2 fenc=utf-8 expandtab
autocmd FileType php :nmap ,l :call PHPLint()<CR>
autocmd FileType ruby set tabstop=2 tw=0 sw=2 expandtab
autocmd FileType eruby set tabstop=2 tw=0 sw=2 expandtab
autocmd FileType html set tabstop=2 expandtab
autocmd FileType javascript set tabstop=2 tw=0 sw=2 fenc=utf-8 expandtab 
"autocmd FileType javascript :compiler gjslint
autocmd FileType coffee setlocal dictionary=$HOME/dotfiles/vimfiles/javascript.dict,$HOME/dotfiles/vimfiles/jQuery.dict
autocmd FileType javascript,coffee setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType coffee set tabstop=2 shiftwidth=2 fenc=utf-8 expandtab
autocmd BufWritePost *.coffee silent CoffeeMake! -c | cwindow | redraw!
autocmd FileType python set tabstop=4 tw=0 sw=4 fenc=utf-8 expandtab
autocmd FileType cpp set tabstop=4 tw=0 sw=4 fenc=utf-8 expandtab
autocmd QuickfixCmdPost make copen

:colorscheme molokai
