#!/bin/sh

git config --global user.name "Takaaki Noguchi"
git config --global user.email takaaki.noguchi@gmail.com

git submodule update --init vimfiles/neobundle.vim/
ln -s $HOME/dotfiles/_vimrc $HOME/.vimrc
ln -s $HOME/dotfiles/vimfiles $HOME/.vim
if [ -f $HOME/.bashrc ] ;then
	mv $HOME/.bashrc $HOME/bashrc_org
fi
ln -s $HOME/dotfiles/_bashrc $HOME/.bashrc
ln -s $HOME/dotfiles/_screenrc $HOME/.screenrc
ln -s $HOME/dotfiles/_tmux.conf $HOME/.tmux.conf
ln -s $HOME/dotfiles/_gitconfig $HOME/.gitconfig

#git clone git://github.com/sstephenson/rbenv.git ~/.rbenv
#mkdir -p ~/.rbenv/plugins
#git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins

